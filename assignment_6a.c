#include<stdio.h>
int main()
{
    int n, sum=0;
    printf("Enter the value of n:\t");
    scanf(" %d",&n);
    for(int i=1;i<=n;i++)
    {
        if(i%2==0)
            sum+=i*i;
    }
    printf("Sum of square of first %d natural numbers is %d",n,sum);
    return 0;
}