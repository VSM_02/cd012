#include<stdio.h>
int main()
{
   /* 2D array declaration*/
   int r,c,ele[100][100],i,j;
   printf("Enter number of rows and columns:..");
   scanf("%d%d",&r,&c);

   for(i=0; i<r; i++)
    {
      for(j=0;j<c;j++)
      {
         printf("Enter value for element[%d][%d]:", i, j);
         scanf("%d", &ele[i][j]);
      }
   }
   //Displaying array elements
   printf("Two Dimensional array elements:\n");
   for(i=0; i<r; i++)
    {
      for(j=0;j<c;j++)
      {
         printf("%d ", ele[i][j]);
      }
            printf("\n");
    }

   return 0;
}
