#include<stdio.h>
#include<math.h>
int main()
{
    float a,b,c,d,r1,r2,r;
    int flag;
    printf("Enter the value of coefficients a b and c\n");
    scanf(" %f %f %f",&a,&b,&c);
    d = b*b - 4*a*c;
    if(d<0)
        flag = 1;
    else if(d>0)
        flag = 2;
    else if(d==0)
        flag = 3;


    switch(flag)
    {
        case 1   :printf("roots are imaginary\n");
            break;
        case 2   :printf("roots are real and distinct\n");
            r1 = (-b +sqrt(d)) / 2*a;
            r2 = (-b -sqrt(d)) / 2*a;
            printf("roots are %.3f and %.3f\n",r1,r2);
            break;
        case 3  :printf("roots are real and equal\n");
            r = -b/2*a;
            printf(" %.3f\n",r);
            break;       
    }
    return 0;
}
