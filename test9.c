#include <stdio.h>

typedef struct student
{
        char firstName[100];
        char lastName[100];
        long roll_no;
        char section[2];
        char department[10];
        int fees;
        int result;
}students;

int main()
{
     students s1,s2;
     printf("-----Enter details of student s1------\n\n");
     printf("Enter first name:------");
     scanf(" %s",s1.firstName);
     printf("Enter last name:-----");
     scanf(" %s",s1.lastName);
     printf("enter roll number:------");
     scanf("%d",&s1.roll_no);
     printf("Enter section:-----");
     scanf("%s",s1.section);
     printf("enter department:-----");
     scanf("%s",s1.department);
     printf("enter fees paid:-----");
     scanf("%d",&s1.fees);
     printf("Enter marks scores:-----");
     scanf("%d",&s1.result);

     printf("\n\n\n\n");

     printf("-----Enter details of student s2------\n\n");
     printf("Enter first name:------");
     scanf(" %s",s2.firstName);
     printf("Enter last name:-----");
     scanf(" %s",s2.lastName);
     printf("enter roll number:------");
     scanf("%d",&s2.roll_no);
     printf("Enter section:-----");
     scanf("%s",s2.section);
     printf("enter department:-----");
     scanf("%s",s2.department);
     printf("enter fees paid:-----");
     scanf("%d",&s2.fees);
     printf("Enter marks scored:-----");
     scanf("%d",&s2.result);

     printf("\n\n\n");

     if(s1.result>s2.result){
        printf("student1 has scored highest marks\n");
        printf("name:----- %s %s\n",s1.firstName,s1.lastName);
        printf("Roll number :----- %ld\n",s1.roll_no);
        printf("section :----- %s\n",s1.section);
        printf("department:----- %s\n",s1.department);
        printf("marks scored:-----%d\n",s1.result);
     }else
     {
        printf("student2 has scored highest marks\n");
        printf("name:----- %s %s\n",s2.firstName,s2.lastName);
        printf("Roll number :----- %ld\n",s2.roll_no);
        printf("section :----- %s\n",s2.section);
        printf("department:----- %s\n",s2.department);
        printf("marks scored:-----%d\n",s2.result);
     }
    return 0;
}
