#include <stdio.h>
int main()
{
    int a,b,temp;
    int* p1 = &a;
    int* p2 = &b;

    printf("Enter values of a and b::");
    scanf("%d %d",&a,&b);

    printf("Before swapping a = %d and b = %d \n",a,b);


    temp = *p1;
    *p1 = *p2;
    *p2 = temp;
    printf("After swapping a = %d; b = %d ",a,b);


    return 0;
}
