#include  <stdio.h>
#include  <stdlib.h>

   int main()
   {
        FILE* f1;
        f1 = fopen("test2.txt","w");
        char c;
        printf("Data Input\n\n");
        while((c=getchar()) != EOF)
             putc(c,f1);
        fclose(f1);

        f1 = fopen("test2.txt","r");
        printf("\nData Output\n\n");
        while((c=getc(f1)) != EOF)
            printf("%c",c);
        fclose(f1);

        return 0;
   }
