#include<stdio.h>
#include<math.h>

float input();
void decide(float d, float a, float b, float c);
float calculate(float a, float b, float c);

int main()
{
    float a,b,c,d;
    printf("Enter the three coefficients\n");
    a = input();
    b = input();
    c = input();

    d = calculate(a,b,c);
    decide(d,a,b,c);
    return 0;
}

float input()
{
    float a;
    scanf("%f",&a);
    return a;
}
float calculate(float a, float b, float c)
{
    float d = (pow(b,2) - 4*a*c);
    return d;
}
void decide(float d, float a, float b, float c)
{
    if(d==0)
    {
        float r = -b/2*a;
        printf("Root =  %.3f",r);
    }
    else if(d<0)
    {
        printf("Roots are imaginary");
        printf("First root = %.2lf + i*%.2lf\n", -b/(double)(2*a), sqrt(-d)/(2*a));
        printf("Second root = %.2lf - i*%.2lf\n", -b/(double)(2*a), sqrt(-d)/(2*a));
    }
    else
    {
        float r1 = (-b+sqrt(d))/(2*a);
        float r2 = (-b-sqrt(d))/(2*a);
        printf("Roots are %.3f %.3f",r1,r2);
    }
}
