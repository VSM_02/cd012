#include<stdio.h>
#include <math.h>

int main()
{
    float a,b,c,d;
    printf("Enter the three coefficients\n");
    scanf(" %f %f %f",&a,&b,&c);
    d = pow(b,2) - 4*a*c;

    if(d==0){
        float r = -b/2*a;
        printf("Root =  %.3f",r);
    }else if(d<0){
        printf("Roots are imaginary");
    }else{
        float r1 = (-b+sqrt(d))/(2*a);
        float r2 = (-b-sqrt(d))/(2*a);
        printf("Roots are %.3f %.3f",r1,r2);

    }
    return 0;
}
